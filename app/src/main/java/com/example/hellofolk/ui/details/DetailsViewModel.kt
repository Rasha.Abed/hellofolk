package com.example.hellofolk.ui.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.hellofolk.model.data.Comment
import com.example.hellofolk.model.repository.CommentRepository

class DetailsViewModel : ViewModel() {


    private val commentRepository = CommentRepository(viewModelScope)

    val commentApiResponseLD = commentRepository.commentByIdApiResponse

    fun getCommentsById(id: Int) {
        commentRepository.getCommentsById(id)
    }

    fun getCommentsFromDB(id: Int): LiveData<List<Comment>>{
        return commentRepository.getCommentsFromDB(id)
    }



}