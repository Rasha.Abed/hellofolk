package com.example.hellofolk.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagedList
import com.example.hellofolk.model.data.Post
import com.example.hellofolk.model.repository.PostsRepository
import com.example.hellofolk.model.repository.db.entityModels.UserWithPost

class HomeViewModel : ViewModel() {
   private val postsRepository = PostsRepository(viewModelScope)

    val postApiResponseLD =postsRepository.postApiResponse

    fun getPosts(){
        postsRepository.getPosts()
    }

    fun loadPostsPaged() {
        postsRepository.loadPostsPaged()
    }
    fun getPaged(): LiveData<PagedList<UserWithPost>> {
       return postsRepository.postsDB

    }
}