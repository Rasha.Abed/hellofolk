package com.example.hellofolk.ui.details

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.view.View
import com.example.hellofolk.R
import com.example.hellofolk.adapter.CommentAdapter
import com.example.hellofolk.adapter.PostAdapter
import com.example.hellofolk.databinding.DetailsFragmentBinding
import com.example.hellofolk.model.data.Comment
import com.example.hellofolk.model.data.Post
import com.example.hellofolk.ui.base.BaseFragment

class DetailsFragment : BaseFragment<DetailsFragmentBinding,DetailsViewModel>(),View.OnClickListener  {

    lateinit var currentPost: Post

    override fun getResourceId()= R.layout.details_fragment

    override fun createViewModel()= ViewModelProvider(this).get(DetailsViewModel::class.java)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            currentPost = DetailsFragmentArgs.fromBundle(it).post
        }


        viewModel.getCommentsFromDB(currentPost.id).observe(this){
            binding.comments = it



        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.post = currentPost
        binding.executePendingBindings()

    }

    override fun onClick(v: View?) {
    }


}