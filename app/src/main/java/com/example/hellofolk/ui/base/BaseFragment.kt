package com.example.hellofolk.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import com.example.hellofolk.BR
import com.example.hellofolk.utils.ItemClickListener

/**
 * Created by Rasha Abed on 06/06/2021 @08:58 PM.
 */
abstract class BaseFragment<B : ViewDataBinding, VM : ViewModel> : Fragment() {

    protected lateinit var navController: NavController

    @LayoutRes
    abstract fun getResourceId(): Int

    abstract fun createViewModel(): VM

    open fun getOnItemClickListener(): ItemClickListener? = null
    protected lateinit var viewModel: VM
    protected lateinit var binding: B

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = createViewModel()

    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, getResourceId(), container, false)
        binding.setVariable(BR.viewModel, viewModel)
        binding.setVariable(BR.listener, getOnItemClickListener())
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = view.findNavController()

    }


}