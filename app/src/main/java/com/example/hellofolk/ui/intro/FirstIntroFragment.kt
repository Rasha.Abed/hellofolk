package com.example.hellofolk.ui.intro

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.hellofolk.R

class FirstIntroFragment : Fragment() {

    companion object {
        fun newInstance() = FirstIntroFragment()
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.first_intro_fragment, container, false)
    }



}