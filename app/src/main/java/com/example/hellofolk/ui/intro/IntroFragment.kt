package com.example.hellofolk.ui.intro

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.example.hellofolk.HFApp
import com.example.hellofolk.R
import com.example.hellofolk.adapter.IntroPagerAdapter
import com.example.hellofolk.databinding.IntroFragmentBinding
import com.example.hellofolk.ui.base.BaseFragment

class IntroFragment : BaseFragment<IntroFragmentBinding, IntroViewModel>(), View.OnClickListener {
    private lateinit var adapter: IntroPagerAdapter

    override fun getResourceId() = R.layout.intro_fragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        adapter = IntroPagerAdapter(this)
    }

    override fun createViewModel() = ViewModelProvider(this).get(IntroViewModel::class.java)

    override fun onClick(v: View?) {
        when (v?.id) {
            binding.forwardImgV.id -> {
                if (binding.backImgV.visibility == View.VISIBLE) {
                    HFApp.instance.isIntroShowed = true
                    HFApp.instance.sharedPreferences
                        .edit()
                        .putBoolean("isIntroShowed", true)
                        .apply()
                    kotlin.runCatching {
                        navController.navigate(IntroFragmentDirections.actionNavIntroToNavHome())
                    }
                } else {
                    binding.backImgV.visibility = View.VISIBLE
                    binding.bodyVP.currentItem = binding.bodyVP.currentItem + 1
                }

            }
            binding.backImgV.id -> {
                binding.backImgV.visibility = View.INVISIBLE
                binding.bodyVP.currentItem = binding.bodyVP.currentItem - 1

            }
        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.bodyVP.adapter = adapter

        binding.forwardImgV.setOnClickListener(this)
        binding.backImgV.setOnClickListener(this)
    }

}