package com.example.hellofolk.ui.splash

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavDirections
import com.example.hellofolk.HFApp
import com.example.hellofolk.R
import com.example.hellofolk.databinding.SplashFragmentBinding
import com.example.hellofolk.ui.base.BaseFragment
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

class SplashFragment : BaseFragment<SplashFragmentBinding, SplashViewModel>(),
    View.OnClickListener {

    override fun getResourceId() = R.layout.splash_fragment
    override fun createViewModel() = ViewModelProvider(this).get(SplashViewModel::class.java)

    override fun onClick(v: View?) {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val currentTime = System.currentTimeMillis()
        viewModel.getComments()


        if (HFApp.instance.isIntroShowed)
            navigate(SplashFragmentDirections.actionNavSplashToNavHome(), currentTime)
        else {
            viewModel.getUsers()

            navigate(SplashFragmentDirections.actionNavSplashToNavIntro(), currentTime)
        }


    }

    private fun navigate(destination: NavDirections, firstTime: Long) {
        val elapsedTime = System.currentTimeMillis() - firstTime
        Executors.newSingleThreadScheduledExecutor().schedule({
            requireActivity().runOnUiThread {
                try {
                    navController.navigate(destination)
                } catch (e: Exception) {
                }
            }
        }, if (3500 - elapsedTime > 0) 3500 - elapsedTime else 0, TimeUnit.MILLISECONDS)
    }

}