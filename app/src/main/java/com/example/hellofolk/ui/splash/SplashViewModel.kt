package com.example.hellofolk.ui.splash

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.hellofolk.model.repository.CommentRepository
import com.example.hellofolk.model.repository.UsersRepository

class SplashViewModel : ViewModel() {
    private val usersRepository = UsersRepository(viewModelScope)

    val userApiResponseLD =usersRepository.userApiResponse

    fun getUsers(){
        usersRepository.getUsers()
    }

    private val commentRepository = CommentRepository(viewModelScope)

    val commentsApiResponseLD =commentRepository.commentApiResponse

    fun getComments(){
        commentRepository.getComments()
    }
}