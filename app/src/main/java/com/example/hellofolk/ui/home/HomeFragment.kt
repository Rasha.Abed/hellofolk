package com.example.hellofolk.ui.home

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.hellofolk.R
import com.example.hellofolk.adapter.PagedPostAdapter
import com.example.hellofolk.adapter.PostAdapter
import com.example.hellofolk.databinding.HomeFragmentBinding
import com.example.hellofolk.model.data.Post
import com.example.hellofolk.ui.base.BaseFragment
import com.example.hellofolk.utils.ItemClickListener

class HomeFragment : BaseFragment<HomeFragmentBinding, HomeViewModel>(), View.OnClickListener {
    private lateinit var postAdapter: PostAdapter
    private lateinit var adapter: PagedPostAdapter

    override fun getResourceId() = R.layout.home_fragment

    override fun createViewModel() = ViewModelProvider(this).get(HomeViewModel::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.getPosts()
        postAdapter = PostAdapter(getOnItemClickListener())

        adapter = PagedPostAdapter(getOnItemClickListener())
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.postsRV.layoutManager = LinearLayoutManager(context)
        binding.postsRV.adapter = adapter
        binding.postsRV.setHasFixedSize(true)
        viewModel.postApiResponseLD.observe(viewLifecycleOwner) { listAPIResponse ->
            if (listAPIResponse.success){

                viewModel.loadPostsPaged()
            }
            else if(listAPIResponse.hasError){
                Toast.makeText(context, "Something wrong!", Toast.LENGTH_SHORT).show()

            }
        }

        viewModel.getPaged().observe(viewLifecycleOwner) { list ->
            adapter.submitList(list)

        }

    }

    override fun getOnItemClickListener(): ItemClickListener? {
        return object : ItemClickListener {
            override fun onClick(post: Post) {
                navController.navigate(HomeFragmentDirections.actionNavHomeToNavDetails(post))
            }

        }
    }

    override fun onClick(v: View?) {
    }


}