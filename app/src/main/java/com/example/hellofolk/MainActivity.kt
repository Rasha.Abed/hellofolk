package com.example.hellofolk

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import com.example.hellofolk.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding:ActivityMainBinding
    private lateinit var mAppBarConfiguration: AppBarConfiguration


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        mAppBarConfiguration = AppBarConfiguration.Builder(
            R.id.nav_home
        ).build()


        val navController = findNavController(R.id.nav_host_fragment)
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration)
        navController.addOnDestinationChangedListener { controller, destination, arguments ->
            when (destination.id) {
                R.id.nav_home, R.id.nav_details -> {
                    binding.appBar.visibility = View.VISIBLE
                    window.decorView.systemUiVisibility = View.VISIBLE


//                    binding.toolbarLogo.visibility = View.VISIBLE
//                    toolbar.visibility = View.VISIBLE

                }
                else -> {
                    binding.appBar.visibility = View.GONE
                    window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN

//                    binding.toolbarLogo.visibility = View.GONE
//                    toolbar.visibility = View.GONE
                }
            }
        }
    }



    override fun onSupportNavigateUp(): Boolean {
        val navController = Navigation.findNavController(this, R.id.nav_host_fragment)
        return (NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp())
    }
}