package com.example.hellofolk.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.hellofolk.BR
import com.example.hellofolk.R
import com.example.hellofolk.databinding.ItemPostBinding
import com.example.hellofolk.model.data.Post
import com.example.hellofolk.model.repository.db.entityModels.UserWithPost
import com.example.hellofolk.utils.ItemClickListener

/**
 * Created by Rasha Abed on 08/06/2021 @10:47 AM.
 */
class PagedPostAdapter(
    private val itemClickListener: ItemClickListener?

) :
    PagedListAdapter<UserWithPost, PagedPostAdapter.PostHolder>(NewsDiffCallback) {

    companion object {
        val NewsDiffCallback = object : DiffUtil.ItemCallback<UserWithPost>() {
            override fun areItemsTheSame(oldItem: UserWithPost, newItem: UserWithPost): Boolean {
                return oldItem.post.title == newItem.post.title
            }

            override fun areContentsTheSame(oldItem: UserWithPost, newItem: UserWithPost): Boolean {
                return oldItem == newItem
            }
        }
    }


    class PostHolder(binding: ItemPostBinding) : RecyclerView.ViewHolder(binding.getRoot()) {
        var binding = binding


        fun bindTo(post: Post) {
            binding.item = post
            binding.executePendingBindings()
        }


    }

    override fun onBindViewHolder(holder: PostHolder, position: Int) {
        getItem(position)?.let { holder.bindTo(it.getFullPost()) }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostHolder {

        val binding: ItemPostBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_post,
            parent,
            false
        )
        binding.listener = itemClickListener


        return PostHolder(binding)
    }

}