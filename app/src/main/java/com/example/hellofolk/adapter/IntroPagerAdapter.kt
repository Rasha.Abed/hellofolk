package com.example.hellofolk.adapter

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.hellofolk.ui.intro.FirstIntroFragment
import com.example.hellofolk.ui.intro.SecondIntroFragment

/**
 * Created by Rasha Abed on 07/06/2021 @12:24 PM.
 */
class IntroPagerAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {
    private val NUM = 2


    override fun getItemCount(): Int {
        return NUM
    }

    override fun createFragment(position: Int): Fragment {
        return if (position == 0)
            FirstIntroFragment()
        else
            SecondIntroFragment()

    }


}