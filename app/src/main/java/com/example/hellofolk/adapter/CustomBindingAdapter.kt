package com.example.hellofolk.adapter

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.hellofolk.model.data.Comment
import com.example.hellofolk.model.data.Post
import com.example.hellofolk.utils.ItemClickListener

/**
 * Created by Rasha Abed on 07/06/2021 @03:00 PM.
 */


@BindingAdapter("posts", "listener", requireAll = false)
fun setPosts(
    rv: RecyclerView,
    items: List<Post>?,
    listener: ItemClickListener?
) {
    val adapter = PostAdapter(listener)
    if (items != null) {
        adapter.data = items
    }
    rv.adapter = adapter
}


@BindingAdapter("comments", requireAll = false)
fun setComments(
    rv: RecyclerView,
    items: List<Comment>?,
) {
    val adapter = CommentAdapter()
    if (items != null) {
        adapter.data = items
    }
    rv.adapter = adapter
}
