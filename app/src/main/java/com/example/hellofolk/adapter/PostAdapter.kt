package com.example.hellofolk.adapter

import com.example.hellofolk.R
import com.example.hellofolk.model.data.Post
import com.example.hellofolk.utils.ItemClickListener

/**
 * Created by Rasha Abed on 06/06/2021 @06:25 PM.
 */
class PostAdapter(
    itemClickListener: ItemClickListener?
) : BaseAdapter<Post>(itemClickListener, R.layout.item_post) {

}