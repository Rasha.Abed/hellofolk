package com.example.hellofolk.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.example.hellofolk.BR
import com.example.hellofolk.utils.ItemClickListener

/**
 * Created by Rasha Abed on 06/06/2021 @07:57 PM.
 */
abstract class BaseAdapter<T>(
    private val itemClickListener: ItemClickListener?,
    @LayoutRes private val resId: Int,
) :
    RecyclerView.Adapter<BaseAdapter<T>.BaseViewHolder>() {
    var data = listOf<T>()

    inner class BaseViewHolder(private val binding: ViewDataBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: T) {
            binding.setVariable(BR.item, item)
            binding.setVariable(BR.listener, itemClickListener)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val binding: ViewDataBinding =
            DataBindingUtil.inflate(LayoutInflater.from(parent.context), resId, parent, false)

        return BaseViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.bind(data[position])
        holder.itemView.setOnClickListener {
            notifyItemChanged(position)
        }
    }

    override fun getItemCount(): Int = data.size
}