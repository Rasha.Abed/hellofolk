package com.example.hellofolk

import android.app.Application
import android.content.SharedPreferences
import android.preference.PreferenceManager
import androidx.multidex.MultiDexApplication

/**
 * Created by Rasha Abed on 07/06/2021 @05:43 PM.
 */
class HFApp: MultiDexApplication()  {

    var isIntroShowed = false
    lateinit var sharedPreferences: SharedPreferences


    companion object {
        lateinit var instance: HFApp

    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        sharedPreferences = PreferenceManager
            .getDefaultSharedPreferences(instance)
        isIntroShowed = sharedPreferences
            .getBoolean("isIntroShowed", false)
    }
}