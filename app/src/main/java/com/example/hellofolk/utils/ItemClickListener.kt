package com.example.hellofolk.utils

import com.example.hellofolk.model.data.Post

/**
 * Created by Rasha Abed on 06/06/2021 @07:06 PM.
 */
interface ItemClickListener {
    fun onClick(post: Post) {
        //noop
    }
}