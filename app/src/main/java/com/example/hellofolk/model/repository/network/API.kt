package com.example.hellofolk.model.repository.network

import com.example.hellofolk.model.data.Comment
import com.example.hellofolk.model.data.Post
import com.example.hellofolk.model.data.User
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Rasha Abed on 06/06/2021 @11:41 PM.
 */
interface API {
    @GET("posts")
    fun getPosts(): Call<List<Post>>

    @GET("users")
    fun getUsers(): Call<List<User>>

    @GET("comments")
    fun getCommentsById(
        @Query("postId") postId: Int
    ): Call<List<Comment>>

    @GET("comments")
    fun getComments(
    ): Call<List<Comment>>
}