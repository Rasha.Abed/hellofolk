package com.example.hellofolk.model.repository

import androidx.lifecycle.LiveData
import com.example.hellofolk.HFApp
import com.example.hellofolk.model.data.Comment
import com.example.hellofolk.model.repository.db.dao.CommentDao
import com.example.hellofolk.model.repository.db.HelloFolkDatabase
import com.example.hellofolk.model.repository.network.APIResponse
import com.example.hellofolk.model.repository.network.NetworkBoundResource
import com.example.hellofolk.model.repository.network.RetrofitClient
import kotlinx.coroutines.CoroutineScope
import retrofit2.Call

/**
 * Created by Rasha Abed on 07/06/2021 @01:22 PM.
 */
class CommentRepository(val scope: CoroutineScope) {
    private val database = HelloFolkDatabase.getDatabase(HFApp.instance, scope)
    private val commentDao: CommentDao = database.commentDao()

    private var postId: Int = 0

    private var networkBoundResourceCommentById =
        object : NetworkBoundResource<List<Comment>>(true, true, false, scope) {
            override fun createCall(): Call<List<Comment>>? {
                return RetrofitClient.instance?.getApi()?.getCommentsById(postId)
            }

        }

    private var _commentByIdApiResponse = networkBoundResourceCommentById.getAsLiveData()
    val commentByIdApiResponse: LiveData<APIResponse<List<Comment>?>>
        get() = _commentByIdApiResponse

    fun getCommentsById(id: Int) {
        postId = id
        networkBoundResourceCommentById.runRequest()
    }


    private var networkBoundResourceComment =
        object : NetworkBoundResource<List<Comment>>(true, true, false, scope) {
            override fun createCall(): Call<List<Comment>>? {
                return RetrofitClient.instance?.getApi()?.getComments()
            }

            override fun saveCallResult(item: List<Comment>) {
                database.commentDao().insert(item)

            }

        }

    private var _commentApiResponse = networkBoundResourceCommentById.getAsLiveData()
    val commentApiResponse: LiveData<APIResponse<List<Comment>?>>
        get() = _commentApiResponse

    fun getComments() {
        networkBoundResourceComment.runRequest()
    }

    fun getCommentsFromDB(id: Int): LiveData<List<Comment>> {
        return commentDao.loadByPostId(id)
    }

}