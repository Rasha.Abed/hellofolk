package com.example.hellofolk.model.repository.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.example.hellofolk.model.data.Comment

/**
 * Created by Rasha Abed on 07/06/2021 @01:08 AM.
 */
@Dao
interface CommentDao: BaseDao<Comment> {

    @Query("SELECT * FROM comment_table WHERE postId = :id")
    fun loadByPostId(id: Int): LiveData<List<Comment>>

    @Query("SELECT * FROM comment_table WHERE postId = :id")
    fun load(id: Int): Comment

}