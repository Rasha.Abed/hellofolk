package com.example.hellofolk.model.data

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import java.io.Serializable

/**
 * Created by Rasha Abed on 06/06/2021 @06:02 PM.
 */
@Entity(
    tableName = "post_table",
)
data class Post(
    @PrimaryKey
    @ColumnInfo(name = "id")
    val id:Int,
    val userId: Int,
    val title: String,
    val body: String
):Serializable {

    @Ignore
    var user: User? = null
}