package com.example.hellofolk.model.data

import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

/**
 * Created by Rasha Abed on 06/06/2021 @06:02 PM.
 */

@Entity(
    tableName = "user_table",
)
data class User(
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "id")
    val id: Int,
    val name: String,
    val email: String,
    val phone: String,
    val website: String,
) {

    @Ignore
    val address: Address? =null
    @Ignore
    val company: Company? = null


}