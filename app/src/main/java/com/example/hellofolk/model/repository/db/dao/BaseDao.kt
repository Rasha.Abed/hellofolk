package com.example.hellofolk.model.repository.db.dao

import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Update

/**
 * Created by Rasha Abed on 07/06/2021 @12:57 AM.
 */
interface BaseDao<T> {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(item: T)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(item: List<T>)

    @Update
    fun update(item: T)



}