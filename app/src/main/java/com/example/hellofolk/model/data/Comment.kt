package com.example.hellofolk.model.data

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by Rasha Abed on 06/06/2021 @06:02 PM.
 */

@Entity(
    tableName = "comment_table",
)
data class Comment (
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "id")
    val id:Int,
    val postId:Int,
    val name: String,
    val email: String,
    val body: String

    ){
}