package com.example.hellofolk.model.repository.db.entityModels

import androidx.room.Embedded
import androidx.room.Relation
import com.example.hellofolk.model.data.Post
import com.example.hellofolk.model.data.User

/**
 * Created by Rasha Abed on 08/06/2021 @12:29 PM.
 */


data class UserWithPost(
    @Embedded val post: Post,
    @Relation(
        parentColumn = "userId",
        entityColumn = "id"
    )
    val user: User
) {

    fun getFullPost():Post{
        post.user = user
        return post


    }

}