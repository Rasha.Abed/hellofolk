package com.example.hellofolk.model.repository

import androidx.lifecycle.LiveData
import com.example.hellofolk.HFApp
import com.example.hellofolk.model.data.User
import com.example.hellofolk.model.repository.db.HelloFolkDatabase
import com.example.hellofolk.model.repository.network.APIResponse
import com.example.hellofolk.model.repository.network.NetworkBoundResource
import com.example.hellofolk.model.repository.network.RetrofitClient
import kotlinx.coroutines.CoroutineScope
import retrofit2.Call

/**
 * Created by Rasha Abed on 07/06/2021 @01:22 PM.
 */
class UsersRepository (val scope: CoroutineScope){

    private val database = HelloFolkDatabase.getDatabase(HFApp.instance, scope)

    private var networkBoundResourceUsers =
        object : NetworkBoundResource<List<User>>(true, true, false, scope) {
            override fun createCall(): Call<List<User>>? {
                return RetrofitClient.instance?.getApi()?.getUsers()
            }

            override fun saveCallResult(item: List<User>) {
                database.userDao().insert(item)

            }


        }

    private var _userApiResponse = networkBoundResourceUsers.getAsLiveData()
    val userApiResponse: LiveData<APIResponse<List<User>?>>
        get() = _userApiResponse

    fun getUsers() {
        networkBoundResourceUsers.runRequest()
    }

}