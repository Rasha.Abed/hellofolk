package com.example.hellofolk.model.repository.db.dao

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import com.example.hellofolk.model.data.Post
import com.example.hellofolk.model.repository.db.dao.BaseDao
import com.example.hellofolk.model.repository.db.entityModels.UserWithPost

/**
 * Created by Rasha Abed on 07/06/2021 @01:05 AM.
 */

@Dao
interface PostDao : BaseDao<Post> {
    @Query("SELECT * FROM post_table WHERE id = :id")
    fun load(id: Int): Post?

    @Query("SELECT * FROM post_table")
    fun loadAll(): LiveData<List<Post>>

    @Transaction
    @Query("SELECT * FROM POST_TABLE")
    fun loadAllPaged(): DataSource.Factory<Int, UserWithPost>
}