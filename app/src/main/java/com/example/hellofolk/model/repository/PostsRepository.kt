package com.example.hellofolk.model.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import androidx.paging.PagedList
import com.example.hellofolk.HFApp
import com.example.hellofolk.model.data.Post
import com.example.hellofolk.model.repository.db.HelloFolkDatabase
import com.example.hellofolk.model.repository.db.dao.PostDao
import com.example.hellofolk.model.repository.db.entityModels.UserWithPost
import com.example.hellofolk.model.repository.network.APIResponse
import com.example.hellofolk.model.repository.network.NetworkBoundResource
import com.example.hellofolk.model.repository.network.RetrofitClient
import kotlinx.coroutines.CoroutineScope
import retrofit2.Call
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

/**
 * Created by Rasha Abed on 07/06/2021 @01:37 AM.
 */
class PostsRepository(val scope: CoroutineScope) {

    private val database = HelloFolkDatabase.getDatabase(HFApp.instance, scope)
    private val postDao: PostDao = database.postDao()
    private val config: PagedList.Config= PagedList.Config.Builder()
        .setEnablePlaceholders(false)
        .setPrefetchDistance(1)
        .setPageSize(5)
        .build()

    private val pagedListMutableLiveData: MutableLiveData<PagedList<UserWithPost>> =
        MutableLiveData<PagedList<UserWithPost>>()


    private var networkBoundResourcePost =
        object : NetworkBoundResource<List<Post>>(true, true, false, scope) {
            override fun createCall(): Call<List<Post>>? {
                return RetrofitClient.instance?.getApi()?.getPosts()
            }

            override fun saveCallResult(item: List<Post>) {
                super.saveCallResult(item)
                database.postDao().insert(item)

            }
        }

    private var _postApiResponse = networkBoundResourcePost.getAsLiveData()
    val postApiResponse: LiveData<APIResponse<List<Post>?>>
        get() = _postApiResponse

    fun getPosts() {
        networkBoundResourcePost.runRequest()
    }


    fun loadPostsPaged() {
        Executors.newSingleThreadScheduledExecutor().schedule( {
            val factory: DataSource.Factory<Int, UserWithPost> = postDao.loadAllPaged()
            val pagedList: PagedList<UserWithPost> = PagedList.Builder<Int, UserWithPost>(factory.create(), config)
                .setInitialKey(0)
                .setFetchExecutor(Executors.newSingleThreadExecutor())
                .setNotifyExecutor(com.bumptech.glide.util.Executors.mainThreadExecutor())
                .build()
            pagedListMutableLiveData.postValue(pagedList)
        },500,TimeUnit.MILLISECONDS)
    }
    val postsDB: LiveData<PagedList<UserWithPost>>
        get() = pagedListMutableLiveData


}