package com.example.hellofolk.model.repository.network

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Rasha Abed on 07/06/2021 @12:06 AM.
 */
class APIResponse<T>(
    @SerializedName("data")
    val data: T?,
    @Expose(serialize = false,deserialize = false)
    val hasError: Boolean,
    @Expose(serialize = false,deserialize = false)
    val message: String?,
    @Expose(serialize = false, deserialize = false)
    val running: Boolean,
    @Expose(serialize = false, deserialize = false)
    val success: Boolean,
    @Expose(serialize = false, deserialize = false)
    val empty: Boolean
)
{
    @Expose(serialize = false, deserialize = false)
    private var currentpercent = 0

    @Expose(serialize = false, deserialize = false)
    private var totalpercent = 0

    @Expose(serialize = false, deserialize = false)
    private var fileNumber = 0

    companion object {
        fun <T> success(data: T): APIResponse<T> {
            return APIResponse(data, false, null, false, true, false)
        }

        fun <T> error(msg: String?, data: T?): APIResponse<T?> {
            return APIResponse(data, true, msg, false, false, false)
        }

        fun <T> empty(data: T): APIResponse<T> {
            return APIResponse(data, false, null, false, false, true)
        }

        fun <T> loading(data: T?): APIResponse<T?> {
            return APIResponse(data, false, null, true, false, false)
        }

        fun <T> loading(currentpercent: Int, totalpercent: Int, fileNumber: Int): APIResponse<T?> {
            val res = APIResponse<T?>(null, false, null, true, false, false)
            res.currentpercent = currentpercent
            res.totalpercent = totalpercent
            res.fileNumber = fileNumber
            return res
        }
    }
}