package com.example.hellofolk.model.data

/**
 * Created by Rasha Abed on 06/06/2021 @06:08 PM.
 */
data class Address (
    val street: String,
    val suite: String,
    val city: String,
    val zipcode: String,
    val geo: Geo,

    ){
}