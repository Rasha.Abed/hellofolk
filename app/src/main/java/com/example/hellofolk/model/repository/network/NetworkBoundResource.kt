package com.example.hellofolk.model.repository.network

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by Rasha Abed on 06/06/2021 @11:41 PM.
 */
abstract class NetworkBoundResource<ResultType>(
    val shouldFetchFromNetwork: Boolean,
    val shouldSaveToDB: Boolean,
    val shouldFetchFromDB: Boolean,
    private val scope: CoroutineScope
) {
    private val result = MediatorLiveData<APIResponse<ResultType?>>()

    private fun fetchFromNetwork(dbSource: LiveData<ResultType>?) {
        if (shouldFetchFromDB) result.addSource(
            dbSource!!
        ) { newData: ResultType ->
            result.setValue(
                APIResponse.loading(newData)
            )
        } else result.setValue(APIResponse.loading(null))
        createCall()?.enqueue(object : Callback<ResultType> {
            override fun onResponse(
                call: Call<ResultType>,
                response: Response<ResultType>
            ) {
                if (shouldFetchFromDB) result.removeSource(dbSource!!)
                if (response.body() == null || !response.isSuccessful) {

                    if (shouldFetchFromDB) result.addSource(
                        dbSource!!
                    ) { newData: ResultType ->
                        result.setValue(
                            APIResponse.error(
                                "Error " + response.code() + " " + response.message(),
                                newData
                            )
                        )
                    } else result.setValue(
                        APIResponse.error(
                            "Error " + response.code() + " " + response.message(),
                            null
                        )
                    )
                } else {
                    response.body()?.let { body ->
                            if (shouldSaveToDB) {
                                saveResultAndReInit(body)
                                if (!shouldFetchFromDB) result.setValue(
                                    APIResponse.success(
                                        body
                                    )
                                )
                            } else {
                                if (shouldFetchFromDB) result.addSource(
                                    dbSource!!
                                ) { newData: ResultType ->
                                    result.setValue(
                                        APIResponse.success(newData)
                                    )
                                } else result.setValue(
                                    APIResponse.success(
                                        body
                                    )
                                )
                            }
                        }


                }
            }

            override fun onFailure(call: Call<ResultType>, t: Throwable) {

                if (shouldFetchFromDB) {
                    dbSource?.let {
                        result.removeSource(dbSource)

                        result.addSource(dbSource) { newData: ResultType ->
                            result.setValue(
                                APIResponse.error(t.message, newData)
                            )
                        }
                    }
                } else {
                    onFetchFailed(t.localizedMessage)
                }
            }
        })
    }

    fun saveResultAndReInit(response: ResultType) {
        scope.launch(Dispatchers.IO) {
            saveCallResult(response)
        }
        if (shouldFetchFromDB) {
            result.addSource(
                loadFromDb()
            ) { newData: ResultType ->
                result.setValue(
                    APIResponse.success(newData)
                )
            }
        }

    }


    open fun saveCallResult(item: ResultType) {}
    open fun onFetchFailed(errMsg: String?) {
        result.setValue(
            APIResponse.error(errMsg, null)
        )
    }

    open fun loadFromDb(): LiveData<ResultType> = MutableLiveData()
    abstract fun createCall(): Call<ResultType>?
    fun getAsLiveData(): MutableLiveData<APIResponse<ResultType?>> {
        return result
    }

    fun runRequest() {
        if (shouldFetchFromDB) {
            val dbSource = loadFromDb()
            result.addSource(dbSource) {
                result.removeSource(dbSource)
                if (shouldFetchFromNetwork) {
                    fetchFromNetwork(dbSource)
                } else {
                    result.addSource(
                        dbSource
                    ) { newData: ResultType ->
                        result.setValue(
                            APIResponse.success(newData)
                        )
                    }
                }
            }
        } else {
            fetchFromNetwork(null)
        }
    }

}