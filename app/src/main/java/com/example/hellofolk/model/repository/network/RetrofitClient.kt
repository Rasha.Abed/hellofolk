package com.example.hellofolk.model.repository.network
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
/**
 * Created by Rasha Abed on 06/06/2021 @11:41 PM.
 */
class RetrofitClient {

    private val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    fun getApi(): API {
        return retrofit.create(API::class.java)
    }

    companion object {
        @get:Synchronized
        var instance: RetrofitClient? = null
            get() {
                if (field == null) field = RetrofitClient()
                return field
            }
            private set
    }
}