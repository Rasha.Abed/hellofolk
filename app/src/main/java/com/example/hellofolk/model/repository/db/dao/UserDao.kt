package com.example.hellofolk.model.repository.db.dao

import androidx.room.Dao
import androidx.room.Query
import com.example.hellofolk.model.data.User
import com.example.hellofolk.model.repository.db.dao.BaseDao

/**
 * Created by Rasha Abed on 07/06/2021 @01:07 AM.
 */
@Dao
interface UserDao: BaseDao<User> {
    @Query("SELECT * FROM user_table WHERE id = :id")
    fun load(id: Int): User?


}