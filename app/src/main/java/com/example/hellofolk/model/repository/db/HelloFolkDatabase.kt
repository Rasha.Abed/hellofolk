package com.example.hellofolk.model.repository.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.hellofolk.model.data.Comment
import com.example.hellofolk.model.data.Post
import com.example.hellofolk.model.data.User
import com.example.hellofolk.model.repository.db.dao.CommentDao
import com.example.hellofolk.model.repository.db.dao.PostDao
import com.example.hellofolk.model.repository.db.dao.UserDao
import kotlinx.coroutines.CoroutineScope

/**
 * Created by Rasha Abed on 07/06/2021 @12:59 AM.
 */

@Database(
    entities = [
        User::class, Comment::class, Post::class],
    version = 1
)
abstract class HelloFolkDatabase :RoomDatabase(){

    abstract fun commentDao(): CommentDao
    abstract fun userDao(): UserDao
    abstract fun postDao(): PostDao

    companion object {
        private var INSTANCE: HelloFolkDatabase? = null

        fun getDatabase(
            context: Context,
            scope: CoroutineScope
        ): HelloFolkDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    HelloFolkDatabase::class.java,
                    "HF_database.db"
                )
                    .fallbackToDestructiveMigration()
//                    .allowMainThreadQueries()
                    .addCallback(DatabaseCallBack(scope))
                    .build()

                INSTANCE = instance
                instance
            }
        }

        private class DatabaseCallBack(private val scope: CoroutineScope) :
            RoomDatabase.Callback() {

            override fun onOpen(db: SupportSQLiteDatabase) {
                super.onOpen(db)
            }

            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
//                INSTANCE?.let { database ->
//                    scope.launch  (Dispatchers.IO) {
//                        populateDatabase(database.countryDao())
//                    }
//                }
            }
//            fun populateDatabase(countryDao: CountryDao) {
//                // Start the app with a clean database every time.
//                // Not needed if you only populate on creation.
//                countryDao.deleteAll()
//                CountryCodePicker(SLFApplication.instance).defaultBackgroundColor
//
//            }

        }
    }

}