package com.example.hellofolk.model.data

/**
 * Created by Rasha Abed on 06/06/2021 @06:18 PM.
 */
data class Company(
    val name:String,
    val catchPhrase: String,
    val bs: String
) {
}