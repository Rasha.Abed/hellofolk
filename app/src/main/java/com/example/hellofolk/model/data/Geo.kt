package com.example.hellofolk.model.data

/**
 * Created by Rasha Abed on 06/06/2021 @06:17 PM.
 */
data class Geo(
    val lat: Double,
    val lng: Double
) {
}