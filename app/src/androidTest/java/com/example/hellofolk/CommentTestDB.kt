package com.example.hellofolk

import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.example.hellofolk.model.data.Comment
import com.example.hellofolk.model.repository.db.HelloFolkDatabase
import com.example.hellofolk.model.repository.db.dao.CommentDao
import org.hamcrest.CoreMatchers
import org.junit.After
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

/**
 * Created by Rasha Abed on 10/06/2021 @07:38 PM.
 */

@RunWith(AndroidJUnit4::class)
class CommentTestDB {
    private lateinit var db: HelloFolkDatabase

    private lateinit var commentDao: CommentDao

    @Before
    fun createDb() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext

        db = Room.inMemoryDatabaseBuilder(
            context, HelloFolkDatabase::class.java
        ).build()
        commentDao = db.commentDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    @Throws(Exception::class)
    fun writeCommentAndRead() {
        val comment =
            Comment(
                1,
                1,
                "id labore ex et quam laborum",
                "Eliseo@gardner.biz",
                "laudantium enim quasi est quidem magnam voluptate ipsam eos\\ntempora quo necessitatibus\\ndolor quam autem quasi\\nreiciendis et nam sapiente accusantium"
            )

        commentDao.insert(comment)
        val byPostId = commentDao.load(1)
        assertThat(byPostId, CoreMatchers.equalTo(comment))

    }


}