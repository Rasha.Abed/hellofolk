package com.example.hellofolk.ui.splash

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.lifecycle.ViewModelStore
import androidx.navigation.Navigation
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.example.hellofolk.HFApp
import com.example.hellofolk.R
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by Rasha Abed on 09/06/2021 @08:19 PM.
 */

@RunWith(AndroidJUnit4::class)
class SplashFragmentTest {
//    @get:Rule
//    val activityRule = ActivityScenarioRule(MainActivity::class.java)


    @Test
    fun launchSplashFragmentAndVerifyUI() {
        val navController = TestNavHostController(
            ApplicationProvider.getApplicationContext()
        )
        navController.setViewModelStore(ViewModelStore())

        // use launchInContainer to launch the fragment with UI
        val scenario = launchFragmentInContainer<SplashFragment>() {

            SplashFragment().also { fragment ->
                fragment.viewLifecycleOwnerLiveData.observeForever { viewLifecycleOwner ->
                    if (viewLifecycleOwner != null) {
                        // The fragment’s view has just been created
                        navController.setGraph(R.navigation.mobile_navigation)
                        navController.setCurrentDestination(R.id.nav_splash)
                        Navigation.setViewNavController(fragment.requireView(), navController)
                    }
                }
            }
        }

        // now use espresso to look for the fragment's text view and verify it is displayed
        Espresso.onView(ViewMatchers.withId(R.id.logoImgV))
            .check(
                ViewAssertions
                    .matches(ViewMatchers.isDisplayed())
            )
        Thread.sleep(3000)
        if (HFApp.instance.isIntroShowed)
            assertEquals(navController.currentDestination?.id, R.id.nav_home)
        else
            assertEquals(navController.currentDestination?.id, R.id.nav_intro)


    }
}