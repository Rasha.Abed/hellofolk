package com.example.hellofolk.ui.home

import androidx.core.os.bundleOf
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.lifecycle.ViewModelStore
import androidx.navigation.Navigation
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.hellofolk.R
import com.example.hellofolk.adapter.PagedPostAdapter
import com.example.hellofolk.model.data.Post
import com.example.hellofolk.ui.details.DetailsFragment
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by Rasha Abed on 08/06/2021 @01:30 PM.
 */

@RunWith(AndroidJUnit4::class)
class HomeFragmentTest {
//    lateinit var testScenario: ActivityScenario<MainActivity>


    //
//    @get:Rule
//    val activityRule = ActivityScenarioRule(MainActivity::class.java)



    @Test
    fun launchHomeFragmentAndVerifyUI() {
        val navController = TestNavHostController(
            ApplicationProvider.getApplicationContext())
        navController.setViewModelStore(ViewModelStore())


        val scenario =   launchFragmentInContainer<HomeFragment>(){

            HomeFragment().also { fragment ->
                fragment.viewLifecycleOwnerLiveData.observeForever { viewLifecycleOwner ->
                    if (viewLifecycleOwner != null) {
                        // The fragment’s view has just been created
                        navController.setGraph(R.navigation.mobile_navigation)
                        navController.setCurrentDestination(R.id.nav_home)
                        Navigation.setViewNavController(fragment.requireView(), navController)
                    }
                }
            }
        }
        Thread.sleep(3000)
        onView(withId(R.id.postsRV))
            .check(
                ViewAssertions
                    .matches(isDisplayed())
            )
            .perform(RecyclerViewActions.actionOnItemAtPosition<PagedPostAdapter.PostHolder>(10,
                click()))

    }


    @Test
    fun launchDetailsFragmentAndVerifyUI() {
        val navController = TestNavHostController(
            ApplicationProvider.getApplicationContext()
        )
        navController.setViewModelStore(ViewModelStore())

        val fragmentArgs = bundleOf("post" to Post(1,1,"sunt aut facere repellat provident occaecati excepturi optio reprehenderit","quia et suscipit\\nsuscipit recusandae consequuntur expedita et cum\\nreprehenderit molestiae ut ut quas totam\\nnostrum rerum est autem sunt rem eveniet architecto"))


        // use launchInContainer to launch the fragment with UI
        val scenario = launchFragmentInContainer<DetailsFragment>(fragmentArgs) {

            DetailsFragment().also { fragment ->
                fragment.viewLifecycleOwnerLiveData.observeForever { viewLifecycleOwner ->
                    if (viewLifecycleOwner != null) {
                        // The fragment’s view has just been created
                        navController.setGraph(R.navigation.mobile_navigation)
                        navController.setCurrentDestination(R.id.nav_details)
                        Navigation.setViewNavController(fragment.requireView(), navController)
                    }
                }
            }
        }

        Thread.sleep(3000)
        Espresso.onView(ViewMatchers.withId(R.id.commentsRV))
            .check(
                ViewAssertions
                    .matches(ViewMatchers.isDisplayed())
            )
        Assert.assertEquals(navController.currentDestination?.id, R.id.nav_details)
        // Go back

        ViewActions.pressBackUnconditionally()
        //scenario.moveToState(Lifecycle.State.RESUMED)
        Assert.assertEquals(navController.currentDestination?.id, R.id.nav_details)
    }




}