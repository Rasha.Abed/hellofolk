package com.example.hellofolk

import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.example.hellofolk.model.data.Post
import com.example.hellofolk.model.repository.db.HelloFolkDatabase
import com.example.hellofolk.model.repository.db.dao.PostDao
import org.hamcrest.CoreMatchers
import org.junit.After
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

/**
 * Created by Rasha Abed on 10/06/2021 @07:44 PM.
 */

@RunWith(AndroidJUnit4::class)
class PostTestDB {

    private lateinit var db: HelloFolkDatabase

    private lateinit var postDao: PostDao


    @Before
    fun createDb() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext

        db = Room.inMemoryDatabaseBuilder(
            context, HelloFolkDatabase::class.java
        ).build()
        postDao = db.postDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    @Throws(Exception::class)
    fun writeCommentAndRead() {
        val post =
            Post(
                1,
                1,
                "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
                "quia et suscipit\\nsuscipit recusandae consequuntur expedita et cum\\nreprehenderit molestiae ut ut quas totam\\nnostrum rerum est autem sunt rem eveniet architecto"
            )
        postDao.insert(post)
        val byPostId = postDao.load(1)
        assertThat(byPostId, CoreMatchers.equalTo(post))
    }
}