package com.example.hellofolk

import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.example.hellofolk.model.data.User
import com.example.hellofolk.model.repository.db.HelloFolkDatabase
import com.example.hellofolk.model.repository.db.dao.UserDao
import org.hamcrest.CoreMatchers
import org.junit.After
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

/**
 * Created by Rasha Abed on 10/06/2021 @04:02 PM.
 */

@RunWith(AndroidJUnit4::class)
class UserTestDB {
    private lateinit var db: HelloFolkDatabase

    private lateinit var userDao: UserDao


    @Before
    fun createDb() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext

        db = Room.inMemoryDatabaseBuilder(
            context, HelloFolkDatabase::class.java
        ).build()
        userDao = db.userDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    @Throws(Exception::class)
    fun writeUserAndRead() {
        val user =
            User(
                1,
                "Leanne Graham",
                "Sincere@april.biz",
                "1-770-736-8031 x56442",
                "hildegard.org"
            )
        userDao.insert(user)
        val byId = userDao.load(1)
        assertThat(byId, CoreMatchers.equalTo(user))
    }
}